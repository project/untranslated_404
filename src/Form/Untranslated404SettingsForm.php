<?php

namespace Drupal\untranslated_404\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Setting form for untranslated 404.
 */
class Untranslated404SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static($container->get('config.factory'));
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->languageManager = $container->get('language_manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'untranslated_404.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'untranslated404_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('untranslated_404.settings');

    $entity = NULL;

    if ($config->get('language_fallback_node')) {
      $entity = $this->entityTypeManager->getStorage('node')
        ->load($config->get('language_fallback_node'));
    }

    $form['language_fallback_node'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Language fallback node'),
      '#description' => $this->t("The node to fall back to when the current content isn't translated."),
      '#default_value' => $entity,
      '#target_type' => 'node',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ($form_state->getValue('language_fallback_node')) {
      $entity = $this->entityTypeManager->getStorage('node')
        ->load($form_state->getValue('language_fallback_node'));

      $has_translation_for_all_languages = TRUE;

      $languages = $this->languageManager->getLanguages();
      foreach ($languages as $language_key => $language) {
        if (!$entity->hasTranslation($language_key)) {
          $has_translation_for_all_languages = FALSE;
        }
      }

      if (!$has_translation_for_all_languages) {
        $form_state->setErrorByName('language_fallback_node', t('The referenced node needs to be translated in all languages'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('untranslated_404.settings')
      ->set('language_fallback_node', $form_state->getValue('language_fallback_node'))
      ->save();
  }

}
