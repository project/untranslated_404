<?php

namespace Drupal\untranslated_404\EventSubscriber;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\EventSubscriber\DefaultExceptionHtmlSubscriber;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Psr\Log\LoggerInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;
use Drupal\Core\Access\AccessManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Url;
use Drupal\Core\Routing\AccessAwareRouterInterface;

/**
 * Subscriber to change the 404 page according to the untranslated 404 settings.
 */
class Untranslated404EventSubscriber extends DefaultExceptionHtmlSubscriber {

  /**
   * The settings config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The access manager.
   *
   * @var \Drupal\Core\Access\AccessManagerInterface
   */
  protected $accessManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Untranslated404EventSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The HTTP kernel.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Symfony\Component\Routing\Matcher\UrlMatcherInterface $access_unaware_router
   *   A router implementation which does not check access.
   * @param \Drupal\Core\Access\AccessManagerInterface $access_manager
   *   The access manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, HttpKernelInterface $http_kernel, LoggerInterface $logger, RedirectDestinationInterface $redirect_destination, UrlMatcherInterface $access_unaware_router, AccessManagerInterface $access_manager, LanguageManagerInterface $language_manager) {
    parent::__construct($http_kernel, $logger, $redirect_destination, $access_unaware_router);
    $this->config = $config_factory->get('untranslated_404.settings');
    $this->accessManager = $access_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected static function getPriority(): int {
    return -40;
  }

  /**
   * Makes a subrequest to retrieve the error page according to the settings.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The event to process.
   */
  public function on404(ExceptionEvent $event) {
    $language = $this->languageManager->getCurrentLanguage()->getId();
    // Check if it is a node and does not have a translation.
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      if ($node instanceof NodeInterface && (!$node->hasTranslation($language) || !$node->getTranslation($language)
        ->isPublished())) {
        // Check if there is a node available to fall back to.
        if (!empty($this->config->get('language_fallback_node'))) {
          $url = Url::fromUri('entity:node/' . $this->config->get('language_fallback_node'));
        }
        else {
          $url = Url::fromRoute('system.404');
        }
        $this->makeSubrequestToUrl($event, $url, Response::HTTP_NOT_FOUND);
      }
    }
  }

  /**
   * Makes a subrequest to retrieve the custom error page.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The event to process.
   * @param \Drupal\Core\Url $url
   *   The url object for which to make a subrequest for this error message.
   * @param int $status_code
   *   The status code for the error being handled.
   */
  protected function makeSubrequestToUrl(ExceptionEvent $event, Url $url, int $status_code) {
    if ($url->isRouted()) {
      $access_result = $this->accessManager->checkNamedRoute($url->getRouteName(), $url->getRouteParameters(), NULL, TRUE);
      $request = $event->getRequest();

      // Merge the custom path's route's access result's cacheability metadata
      // with the existing one (from the master request), otherwise create it.
      if (!$request->attributes->has(AccessAwareRouterInterface::ACCESS_RESULT)) {
        $request->attributes->set(AccessAwareRouterInterface::ACCESS_RESULT, $access_result);
      }
      else {
        $existing_access_result = $request->attributes->get(AccessAwareRouterInterface::ACCESS_RESULT);
        if ($existing_access_result instanceof RefinableCacheableDependencyInterface) {
          $existing_access_result->addCacheableDependency($access_result);
        }
      }

      // Only perform the subrequest if the nid is actually accessible.
      if (!$access_result->isAllowed()) {
        return;
      }
    }
    $this->makeSubrequest($event, $url->toString(), $status_code);
  }

}
